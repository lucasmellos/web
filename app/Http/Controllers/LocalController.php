<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Local;
use Route;
use Request;
class LocalController extends Controller{
    
        protected function criar_local(){
            
            return view('local.adicionaLocal');

        }
        protected function crie_local(){
            $input = Request::all();
            $new['nome'] = $input['nome'];
            $new['endereco'] = $input['endereco'];         
            Local::create($new);

            return redirect('/local/todos-locais');        
        }
        
        public function mostrar_todos(){
        //Atribuindo items toda a tabela e retornando para view ------------------------|
            $locais = Local::all();
            
        return View('local.todosLocais', compact('locais'));
        }

        //Essa pra enviar os dados pra view (GET)
        protected function editar_local(){
            $id = Route::getCurrentRoute()->parameters()['id'];
            $local = Local::where('id','=',$id)->first();
       
            return view('local.editarLocal', compact('local'));
        }
        //Essa pra enviar os dados inseridos na view pro banco(POST)
        protected function edite_local(){
            $input = Request::all();
            
            $id = Route::getCurrentRoute()->parameters()['id'];
            $local = Local::where('id','=',$id)->first();
            
            $local->nome = $input["nome"];
            $local->endereco = $input["endereco"];
            $local->save();
            
            return redirect('/local/todos-locais');        
        }
            
    protected function remover_local(){
        //Atribuindo váriavel item a partir do codigo da url -----------------------------|        
            $id = Route::getCurrentRoute()->parameters()['id'];
            $local = Local::where('id','=',$id)->first();
        
        return view('local.removerLocal', compact('local'));
    }
    
    protected function remova_local(){
        $id = Route::getCurrentRoute()->parameters()['id'];
        $local = Local::where('id','=',$id)->first();
        
        Local::where('id', $local->id)->delete();
        
        return redirect('/local/todos-locais');        
    }
}
