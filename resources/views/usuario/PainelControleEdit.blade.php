@extends('layout.base')

@section ('title')
    <title>Controle | Editar Usuário</title>
@stop

@section ('head')
<style>
    #content{
        height: 400px;
        background-color: white;
        border: 1px solid #e3e3e3;
        border-radius: 15px;
    }
    input{
        margin-bottom: 5px;
        margin-left: -30px;
    }
        
    #bot{
        position: fixed;
        bottom: 10px;
        padding: 5px 10px;
    }
</style>
@stop

@section ('content')
<form method="POST" enctype="multipart/form-data">{{ csrf_field() }}
    <h1 class='subtitle' style="padding-left: -20px">Editar Usuário</h1>
    <div class="col-lg-12">
        <div class="col-lg-4 form-group">
            <label style="padding-bottom: 5px; margin-left: -25px;"><i class="fa fa-user">&nbsp;&nbsp;</i>Nome</label>
            <input id="name" type="text" class="form-control" name="name" placeholder="Nome" value="{{$user->name}}" required autofocus>
        </div>
        <div class="col-lg-4 form-group">
            <label style="padding-bottom: 5px; margin-left: -25px;"><i class="fa fa-user">&nbsp;&nbsp;</i>Sobrenome</label>
            <input type="text" class="form-control" name="lastname" placeholder="Sobrenome" value="{{$user->lastname}}" required autofocus>
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <div class="col-lg-8 form-group">
                <label style="padding-bottom: 5px; margin-left: -25px;"><i class="fa fa-envelope-o">&nbsp;&nbsp;</i>Email</label>
                <input id="email" type="email" class="form-control" placeholder="E-mail" name="email" value="{{$email}}" required>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="form-group" id="bot">
        <button class="btn btn-default" type="submit" id='submit'>Enviar</button>
        <a class="btn btn-primary" href="/user/painel">Cancelar</a>
    </div>
</form>
@stop

