@extends('layout.base')

@section ('title')
    <title>Controle | Buscar Item</title>
@stop

@section ('head')
<style>
    input{
        margin-bottom: 5px;
        width: 100%;
    }
    select{
        margin-bottom: 5px;
        width: 100%;
    }
    select:required:invalid {
        color: #999999;
    }
    option[value=""][disabled] {
        display: none;
    }
    option {
        color: black;
    }
    #direta{
        margin-top: 25px;
    }
    #inputCdg:invalid{
        border-color: red;
    }

</style>
@stop

@section ('content')
<form class="form-horizontal" method="POST" action="/inventario/resultado-item" autocomplete="off">{{ csrf_field() }}    
    <div id="infor" class="col-lg-8 col-md-8 col-sm-12">
        <h1 class="subtitle"><i class="fa fa-search">&nbsp;</i>Pesquisa de Itens</h1>      
        <select class="form-group form-control" name="projeto" required>
            <optgroup label="Projetos">
                <option value="0">Projeto</option>
                @foreach($projetos as $projeto)               
                <option value="{{$projeto->id}}">{{$projeto->nome}}</option>
                @endforeach
            <optgroup>
        </select>
        <select class="form-group form-control" name="categoria" required>
            <optgroup label="Categorias">
                <option value="0">Categoria</option>
                @foreach($categorias as $categoria)
                <option value="{{$categoria->id}}">{{$categoria->nome}}</option>
                @endforeach
            </optgroup>
        </select>
        <select class="form-group form-control" name="local" required>
            <optgroup label="Locais">
                <option value="0">Local</option>
                @foreach($locais as $local)
                <option value="{{$local->id}}">{{$local->nome}}</option>
                @endforeach
            </optgroup>
        </select>
        <div class="col-lg-12 col-md-12 col-sm-12" style="margin-left: -30px;">
            <button type="submit" class="btn btn-primary">Pesquisar</button>
        </div>
    </div>
    <div id="direta" class="col-lg-8 col-md-6 col-sm-4" style="margin-left: -15px;">
        <h1 class="subtitle"><i class="fa fa-search">&nbsp;</i>Pesquisa por código</h1>
        <div class="col-lg-12 col-md-12 col-sm-12" style="margin-left: -15px;">
            <input type="number" class="col-lg-4 form-control" placeholder="Código de Inventário ou de Patrimônio" name="codigo" style="margin-bottom: 15px;">
            <button type="submit" class="btn btn-primary">Pesquisar</button>
        </div>
    </div>
</form>
@stop