<!DOCTYPE html>
<html lang="pt">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/login.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    @yield('head') 
  </head>
<body class="align">
    <header>
        <a href="/home" class="cd-logo"><img src="{{asset('img/logo.png')}}" style="width: 100px;" alt="Logo"></a>
    </header>
    
    @yield('content') 

    <script src="{{asset('js/jquery-2.1.4.js')}}"></script>
    <script src="{{asset('js/jquery.menu-aim.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
</body>
</html>