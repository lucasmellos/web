<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Categoria;
use Route;
use Request;

class CategoriaController extends Controller
{
        protected function criar_categoria(){
            
            return view('categoria.adicionaCategor');

        }

        protected function crie_categoria(){
            $input = Request::all();
            
            $new['nome'] = $input['nome'];
            Categoria::create($new);

            return redirect('/categor/todos-categor');        
        }
        
        public function mostrar_todos(){
            $categorias = Categoria::all();
            
        return View('categoria.todosCategor', compact('categorias'));
    }
        protected function editar_categoria(){

            $id = Route::getCurrentRoute()->parameters()['id'];
            $categoria = Categoria::where('id','=',$id)->first();
       
            return view('categoria.editarCategor', compact('categoria'));
        }
        
        protected function edite_categoria(){
            $input = Request::all();
            
            $id = Route::getCurrentRoute()->parameters()['id'];
            $categoria = Categoria::where('id','=',$id)->first();
            
            $categoria->nome = $input["nome"];
            $categoria->save();
            
            return redirect('/categor/todos-categor');        
        }
            
    protected function remover_categoria(){
        //Atribuindo váriavel item a partir do codigo da url -----------------------------|        
            $id = Route::getCurrentRoute()->parameters()['id'];
            $categoria = Categoria::where('id','=',$id)->first();
        
        return view('categoria.removerCategor', compact('categoria'));
    }
    protected function remova_categoria(){
        $id = Route::getCurrentRoute()->parameters()['id'];
        $categoria = Categoria::where('id','=',$id)->first();
        
        Categoria::where('id', $categoria->id)->delete();
        
        return redirect('/categor/todos-categor');        
    }
}
