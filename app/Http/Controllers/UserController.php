<?php

namespace App\Http\Controllers;

use Auth;
use View;
use Request;
use App\User;
use App\Requisicao;
use App\Local;
use App\Projeto;
use App\Item;

class UserController extends Controller{
    
    public function show_user(){        
        $data= Auth::user();
        
        return View::make('usuario.PainelControle', compact('data'));
    }
    
    protected function editar_user(){
        $user = Auth::user();
        $email = Auth::user()->email;

        return View::make('usuario.PainelControleEdit',compact('user','email'));
    }
    protected function edite_user(){
        $input = Request::all();
        $user = User::where('id', Auth::user()->id)->first();
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->save();
        
        return redirect('/user/painel');
    }    
    protected function minhas_requ(){
        $requisicoes = Requisicao::where('user',Auth::user()->id)->get();
        $locais = Local::all();
        $projetos = Projeto::all();

        return View::make('usuario.minhasReq',compact('requisicoes','locais','projetos'));
    }
    
    protected function devolver_tudo(){
        $requisicoes = Requisicao::where('user', Auth::user()->id)->get();
        
        return View::make('usuario.devolver', compact('requisicoes'));
    }
    protected function devolva_tudo(){
        $requisicoes = Requisicao::where('user', Auth::user()->id)->get();
        $size = Requisicao::where('user', Auth::user()->id)->count();
        
        for($i = 0; $i < $size; $i++){
            $item = Item::where('codigo',$requisicoes[$i]->codigo)->first();
            
            if($item->status == 'Requisitado'){
                $item->status = 'Disponivel';
                $item->save();

                Requisicao::where('codigo', $requisicoes[$i]->codigo)->delete();            
            }
        }
        return redirect('/user/minhas-requ');
    }
    protected function controlUsers(){
        $users = User::all();
        
        return View::make('usuario.users',compact('users'));
    }
}