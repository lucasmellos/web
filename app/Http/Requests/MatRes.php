<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Item;
use App\Reserva_item;

class MatRes extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(Request $request){
        
        $input=Request::all();
        $idR=$input['id'];

        $ri=Reserva_item::where('id_reserva',$idR)->get();

        for($i=0;$i<sizeof($ri);$i++){
            $itens[]=Item::where('id',$ri[$i]->id_item)->get();
        }

        $new['itens']=json_encode($itens);
        return[$new['itens']];
        
    }
    
}