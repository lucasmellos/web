@extends('layout.base')

@section ('title')
    <title>Controle | Cadastrar Categoria</title>
@stop

@section ('head')
<style>
    select{
        margin-bottom: 5px;
        width: 100%;
    }
    select:required:invalid {
        color: #999999;
    }
    option[value=""][disabled] {
        display: none;
    }
    option {
        color: black;
    }
    #bot{
        position: absolute;
        bottom: 10px;
        padding: 5px 10px;
    }
</style>
@stop

@section ('content')
<form class="form-horizontal" method="POST" autocomplete="off" enctype="multipart/form-data">
    {{ csrf_field() }}
    <h1 class="subtitle col-lg-8">Cadastrar Categoria</h1>
    <div class="col-lg-8 col-md-12 col-sm-12" style="margin: 0 auto;">
    <input class="form-group form-control" placeholder="Nome" name="nome" required>            
    </div>
    <div id="bot">
        <button type="submit" class="btn btn-default">Adicionar</button>
        <a class="btn btn-primary" onclick="window.history.back()">Cancelar</a>
    </div>
</form>        
@stop

@section ('script') 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
@stop
