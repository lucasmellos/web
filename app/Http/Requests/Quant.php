<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Reserva;
use App\Item;

class Quant extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(Request $request){
        
        $input=Request::all();
        $materiais=$input['itens'];

        $esc=Item::where('id',$materiais)->first();

        $itens=Item::where('categoria',$esc->categoria)->where('status','Disponivel')->get();

        if($itens!=null && sizeof($itens)>0){
            for($i=0;$i<sizeof($itens);$i++){
                $new['itens']=json_encode($itens);
            }
            return[$new['itens']];
        }else{
            die;
        }
    }
    
}