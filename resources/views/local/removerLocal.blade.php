@extends('layout.base')

@section ('title')
    <title>Controle | Remover Local</title>
@stop

@section ('head')
<style>
    #bot{
        position: fixed;
        bottom: 10px;
        padding: 5px 10px;
    }
    #infor{
        margin-top: 25px;
    }

</style>
@stop

@section ('content')
<h1 class="subtitle col-lg-10">Remover Local</h1>
<form method="POST" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-lg-10">
        <div class="col-lg-12 col-md-9 col-sm-6" id="infor">
            <ul class="list-group panel-default" style="text-align: center">
                <li class="list-group-item">Nome: <b>{{$local->nome}}</b></li>
                <li class="list-group-item">Endereço: {{$local->endereco}}</li>
            </ul>
        </div>
        <div id="bot">
            <button type="submit" class="btn btn-default">Remover</button>
            <a class="btn btn-primary" onclick="window.history.back()">Cancelar</a>
        </div>
    </div>
</form>
@stop
