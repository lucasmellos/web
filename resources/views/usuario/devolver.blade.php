@extends('layout.base')

@section ('title')
    <title>Controle | Devolver</title>
@stop

@section ('head')
<style>
    td, th{
        text-align: center;
    }
    center{
        margin-top: 10%;
        padding: 30px;
        font-size: 20px;
    }
    tr:nth-child(even) {background: #EEE}
    tr:nth-child(odd) {background: #FFF}
</style>
@stop

@section ('content')
<h1 class="subtitle col-lg-10">Devolver - Minhas Requisições</h1>
<div style="overflow-x:auto;" class="col-lg-10">    
    <table class="table">
        <tr>
            <th><i class="fa fa-bookmark-o"></i></th>
            <th>Item</th>
            <th>Código</th>
        </tr>
        @foreach($requisicoes as $requisicao)
        <tr>
            <td>{{$loop->index+1}}</td>
            <td>{{$requisicao->item}}</td>
            <td>{{$requisicao->codigo}}</td>
        </tr>
        @endforeach
    </table>
    <div>
        <form class="form-horizontal" method="POST" autocomplete="off" enctype="multipart/form-data">
        {{ csrf_field() }}
        <button href="#" class="btn btn-default">Devolver</button>
        <a class="btn btn-primary" onclick="window.history.back()">Cancelar</a>
        </form>
    </div>
</div>
@stop