@extends('layout.baseLogin')
@section('content')

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

<form class="form-horizontal col-lg-6 col-md-6 col-sm-6" role="form" method="POST" action="{{ route('password.email') }}">
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email" class="col-md-4 control-label">Endereço de E-Mail</label>

        <div class="col-md-6">
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12" style="text-align: center">
            <button type="submit" class="btn btn-primary" style="margin-right: 5px">
                Enviar link de redefinição
            </button>
            <a href="/" class="btn btn btn-default">Cancelar</a>

        </div>
    </div>
</form>
@stop
