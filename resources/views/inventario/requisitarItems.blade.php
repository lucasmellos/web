@extends('layout.base')

@section ('title')
    <title>Controle | Requisitar</title>
@stop

@section ('head')
<style>
    #bot{
        position: absolute;
        bottom: 10px;
        padding: 5px 10px;
    }

    #lu li{
        margin-bottom: 5px;
    }
    #quant::-webkit-input-placeholder{
        text-align: center;
    }
</style>
@stop

@section ('content')
    <div id="infor" class="col-lg-10 col-md-6 col-sm-4">
        <h1 class="subtitle">Requisitar</h1>
        <ul class="list-group panel-default" id="lu">
            @foreach($items as $item)
            <li>{{$item}}</li>
            @endforeach
        </ul>
    </div>
    <div id="bot">
        <button class="btn btn-default">Solicitar</button>
        <a class="btn btn-primary" href="buscar-item">Cancelar</a>
    </div>
@stop
