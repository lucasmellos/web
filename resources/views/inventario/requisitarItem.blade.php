@extends('layout.base')

@section ('title')
    <title>Controle | Requisitar Item</title>
@stop

@section ('head')
<style>
    #bot{
        position: absolute;
        bottom: 10px;
        padding: 5px 10px;
    }
    #quant{
        margin: 5px 0px 0px 0px;
    }
    #local{
        margin: -15px 0px 0px 0px;
    }
    #lu li{
        margin-bottom: 5px;
    }
    #quant::-webkit-input-placeholder{
        text-align: center;
    }
</style>
@stop

@section ('content')
<form class="form-horizontal" method="POST" autocomplete="off">{{ csrf_field() }}
    <h1 class="subtitle">Requisitar Item</h1>
    <div id="infor" class="col-lg-10 col-md-6 col-sm-4">
        <ul class="list-group panel-default" id="lu">
            <li class="list-group-item">Nome: <b>{{$item->nome}}</b></li>
            <li class="list-group-item">Categoria: {{$categorias->nome}}</li>
            <li class="list-group-item">Projeto: {{$projetos->agencia}}</li>
        </ul>
        <select class="form-group form-control" name="local" required id="local">
            @foreach($locais as $local)
            <option value="{{$local->id}}">{{$local->nome}}</option>
            @endforeach
        </select>
        <input class="form-control" placeholder="Quantidade (Disponivel: {{$quant}})" name="quant" id="quant" type="number" min="1" max="{{$quant}}" required>
    </div>
    <div id="bot">
        <button type="submit" class="btn btn-default">Solicitar</button>
        <a class="btn btn-default" href="/home">Cancelar</a>
    </div>
</form>
@stop
