<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\reserva;

class PageController extends Controller{

    protected function calendario(){
        $data = Reserva::all();
        return view('reserva.calendario', compact('data'));
    }

    protected function agenda(){
        return view('reserva.agenda');
    }
}
