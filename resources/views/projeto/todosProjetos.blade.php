@extends('layout.base')

@section ('title')
    <title>Controle | Projetos</title>
@stop

@section ('head')
<style>
    td, th{
        text-align: center;
    }
    center{
        margin-top: 10%;
        padding: 30px;
        font-size: 20px;   
    }
    #bot{
        position: fixed;
        bottom: 10px;
        right: 0px;
        padding: 0px 50px 10px 0px;
    }
    .dados{
        padding-bottom: 5px;
    }
    tr:nth-child(even) {background: #EEE}
    tr:nth-child(odd) {background: #FFF}
</style>
@stop

@section ('content')
<h1 class="subtitle">Projetos Cadastrados</h1>
<div>
</div>
@if(sizeof($projetos) != 0)
    <div style="overflow-x:auto;">    
        <table class="table">
            <tr>
                <th>Nome</th>
                <th>Agência</th>
                <th>Ano</th>
                <th></th>
            </tr>

            @foreach($projetos as $projeto)
            <tr>
                <td>{{$projeto->nome}}</td>
                <td>{{$projeto->agencia}}</td>
                <td>20{{$projeto->inicio}}</td>
                <td style="text-align: right">
                    <a href="/projeto/{{$projeto->id}}/remover-projeto" data-toggle="tooltip" title="Remover" class="glyphicon glyphicon-trash"></a> 
                    <a href="/projeto/{{$projeto->id}}/editar-projeto" data-toggle="tooltip" title="Editar" class="glyphicon glyphicon-pencil"></a>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
@else
    <center>Nenhum item encontrado!</center>
@endif
@stop

@section ('script')
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
@stop