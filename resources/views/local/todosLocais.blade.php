@extends('layout.base')

@section ('title')
    <title>Controle | Locais</title>
@stop

@section ('head')
<style>
    td, th{
        text-align: center;
    }
    center{
        margin-top: 10%;
        padding: 30px;
        font-size: 20px;   
    }
    .dados{
        padding-bottom: 5px;
    }
    tr:nth-child(even) {background: #EEE}
    tr:nth-child(odd) {background: #FFF}
</style>
@stop

@section ('content')
<h1 class="subtitle">Locais Cadastrados</h1>
<div>
</div>
@if(sizeof($locais) != 0)
    <div style="overflow-x:auto;">    
        <table class="table">
            <tr>
                <th>Nome</th>
                <th>Endereço</th>
                <th></th>
            </tr>

            @foreach($locais as $local)
            <tr>
                <td>{{$local->nome}}</td>
                <td>{{$local->endereco}}</td>
                <td style="text-align: right">
                    <a href="/local/{{$local->id}}/remover-local" data-toggle="tooltip" title="Remover" class="glyphicon glyphicon-trash"></a> 
                    <a href="/local/{{$local->id}}/editar-local" data-toggle="tooltip" title="Editar" class="glyphicon glyphicon-pencil"></a>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
@else
    <center>Nenhum item encontrado!</center>
@endif
@stop

@section ('script')
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
@stop
