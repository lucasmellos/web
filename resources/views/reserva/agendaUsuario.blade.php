@extends('layout.base')

@section ('title')
    <title>Controle | Agendamentos - {{Auth::User()->name}}</title>
@stop

@section ('head')
<style>
    td, th{
        text-align: center;
    }
    center{
        margin-top: 10%;
        padding: 30px;
        font-size: 20px;
    }

    tr:nth-child(even) {background: #EEE}
    tr:nth-child(odd) {background: #FFF}
</style>
@stop

@section ('content')
<h1 class="subtitle" style="border: none; margin-bottom: -10px;">Agendamentos - {{Auth::User()->name}}</h1><br>
@if(sizeof($dataR)==0 ||sizeof($dataR)==0 || sizeof($dataL)==0 || sizeof($dataM)==0)
    <center>Nenhum agendamento!</center>
    @else
    <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" id="x">&times;</button>
            <h4 class="modal-title" style="text-align: center;">Materiais</h4>
        </div>
        <div class="modal-body" style="overflow-x:auto;">
            <table class="table" id="mt">
                <tr>
                    <th>Nome</th>
                    <th>Código</th>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="fechar">Fechar</button>
        </div>
        </div>

    </div>
    </div>
    <div style="overflow-x:auto;">    
        <table class="table" id="res">
            <tr>
                <th></th>
                <th>Local</th>
                <th>Data Início</th>
                <th>Data Final</th>
                <th>Hora Início</th>
                <th>Hora Final</th>
                <th>Materiais</th>
                <th>Quantidade</th>
            </tr>            
                @for($i=0;$i<@sizeof($dataR);$i++)
            <tr id="r">
                <td>
                    <a href="/agendamento/cancelar-agendamento-{{$dataR[$i]->id}}" class="glyphicon glyphicon-trash"></a>
                </td>
                <td>{{$dataL[$i]->nome}}</td>
                <td>{{$dataR[$i]->retirada}}</td>
                <td>{{$dataR[$i]->devolucao}}</td>
                <td>{{$dataR[$i]->hretirada}}</td>
                <td>{{$dataR[$i]->hdevolucao}}</td>
                <td><a href="" id="openModal" data-id="{{$dataR[$i]->id}}" class="glyphicon glyphicon-eye-open" data-toggle="modal" data-target="#myModal"></a></td>
                <td>{{$dataR[$i]->quantidade}}</td>
            </tr>
                @endfor
        </table>
    </div>
@endif
@stop

@section('script')
<script type:"text/javascript">

var t = document.getElementById("res");
var tb= t.getElementsByTagName("tr");
var aux=0;

for(i=1;i<tb.length;i++){
    
    var trs= tb[i];
    var tds= trs.getElementsByTagName("td");
    
    for(j=0;j<tds.length;j++){        
        if(tds[j].getElementsByTagName("a").length>0){
            var td = tds[j].getElementsByTagName("a");
        }
    }

    $(td).on('click', function(){
        var id= $(this).data('id');
        $.ajax({
            method: "get",
            url: "/calendario-MatRes",
            data: {id: id, _token: "{{ csrf_token() }}"},
        
            success: function(retorno){
                var valor= (JSON.parse(retorno));
                console.log(valor);

                for(i=0;i<valor.length;i++){
                    var table = document.getElementById("mt");

                    var row = table.insertRow(1);
                    row.id='valores';
                    var cell1 = row.insertCell(0);
                    cell1.id='n';
                    var cell2 = row.insertCell(1);
                    cell2.id='c';

                    cell1.innerText = valor[i][0].nome;
                    cell2.innerText = valor[i][0].codigo;
                    
                }
                $('#fechar, #x').on('click', function(){
                    for(i=0;i<valor.length;i++){
                        $('#n').remove();
                        $('#c').remove();
                        $('#valores').remove();
                    }
                });

            }
        });
    });
}

</script>
@stop