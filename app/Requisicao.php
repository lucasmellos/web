<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requisicao extends Model
{
    protected $table = 'requisicoes';
    public $timestamps = true;
    
    protected $fillable = [
       'user',
       'codigo',
       'item',
       'quantidade',
       'local',  
    ];
}
