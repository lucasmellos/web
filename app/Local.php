<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class local extends Model
{
    protected $table = 'locals';
    public $timestamps = false;
    
    protected $fillable = [
      'endereco',
      'nome', 
    ];
}
