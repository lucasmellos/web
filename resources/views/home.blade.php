@extends('layout.base')

@section ('title')
    <title>Controle | RExLab</title>
@stop

@section ('head')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel='stylesheet prefetch' href='https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css'>
    <link rel='stylesheet' href="{{asset('css/home.css')}}">
@stop

@section ('content')
<h1 class='subtitle'>Sistema de Controle & Inventário - RExLab</h1>
<div class="counter col-lg-3 col-md-3 col-sm-12">
  <i class="fa fa-laptop fa-2x"></i>
   <p class="count-text ">Itens Cadastrados</p>
  <h2 class="timer count-title count-number" data-to="{{$items}}" data-speed="1500"></h2>
</div>

<div class="counter col-lg-3 col-md-3 col-sm-12">
  <i class="fa fa-briefcase fa-2x"></i>
  <p class="count-text ">Projetos</p>
  <h2 class="timer count-title count-number" data-to="{{$projetos}}" data-speed="1500"></h2>
</div>

<div class="counter col-lg-3 col-md-3 col-sm-12">
  <i class="fa fa-location-arrow fa-2x"></i>
    <p class="count-text ">Locais</p>
    <h2 class="timer count-title count-number" data-to="{{$locais}}" data-speed="1500"></h2>
</div>

@stop
    
@section ('script')
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src='{{asset('js/home.js')}}'></script>
@stop