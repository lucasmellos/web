<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    protected $table = 'reservas';
    public $timestamps = false;
    
    protected $fillable = [
      'devolucao', 'local', 'retirada', 'usuario', 'codigo', 'item', 'hretirada', 'hdevolucao', 'quantidade'
    ];
}
