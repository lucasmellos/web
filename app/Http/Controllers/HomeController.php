<?php

namespace App\Http\Controllers;
use App\Projeto;
use App\Local;
use App\Item;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $projetos = Projeto::count();
        $locais = Local::count();
        $items = Item::count();
        
        return view('home',compact('projetos','locais','items'));
    }
}
