<?php
namespace App\Http\Controllers;

use App\Http\Controllers;
use App\Local;
use Auth;
use App\User;
use App\Item;
use App\Calendario;
use App\Reserva_item;
use Route;
use File;
use Request;
use App\Reserva;


class CalendarioController extends Controller{
    
    public function agendar(){

        $this->mudaDisp();
        $this->mudaDev();

        $materiais = Item::where('status','Disponivel')->get();
        $eventos = Calendario::all();
        $locais = Local::all();
        $usuario = User::all();
        $reservas= Reserva::all();
 
        return view('reserva.calendario', compact('locais','usuario','eventos','materiais','reservas'));
    }

    public function agende(){
        
        $this->mudaDisp();
        $this->mudaDev();

        $input= Request::all();
        $events= fopen("events.json", "a+");
        
        $spc="";
        $array=array(Auth::User()->id,' - ',Auth::User()->name);
        $new['name']=$input['name'];
        $new['criador']=implode($spc,$array);
        $new['startdate']=$input['startdate'];
        $new['enddate']=$input['enddate'];
        $new['starttime']=$input['starttime'];
        $new['endtime']=$input['endtime'];
        $new['local']=$input['local'];
        $new['materiais']=$input['materiais'];
        $new['quantidade']=$input['quant'];
        $new['color']='#1ABC9C';

        Calendario::create($new);

        $req['usuario'] = $new['criador'];
        $req['codigo'] = Item::where('id','=',$new['materiais'])->value('codigo');
        $req['item'] = Item::where('id','=',$new['materiais'])->value('id');
        $req['quantidade']=$new['quantidade'];
        $req['local']=$new['local'];
        $req['retirada']=$new['startdate'];
        $req['devolucao']=$new['enddate'];
        $req['hretirada']=$new['starttime'];
        $req['hdevolucao']=$new['endtime'];

        if($new['quantidade'] != 1){
            $codcomp="";
            $cat=Item::where('id',$new['materiais'])->value('categoria');
            $item= Item::where('status','Disponivel')->where("emprestavel",'1')->where('categoria',$cat)->get();
            for($i=0;$i<$new['quantidade'];$i++){
                $codcomp= $codcomp.$item[$i]->codigo;
                $req['codigo'] = $codcomp;
                
            }
            Reserva::create($req);

            for($j=0;$j<$req['quantidade'];$j++){                
                $res_item['id_item']=$item[$j]->id;
                $res=Reserva::latest('id')->first();
                $res_item['id_reserva']=$res->id;
                $res_item['id_user']=Auth::User()->id;
                Reserva_item::create($res_item);
            }

        }else{
            $item = Item::where('id', $new['materiais'])->first();
            if($item->status == 'Disponivel' && $item->emprestavel == 1){
                Reserva::create($req);
                
                $res_item['id_item']=$item->id;
                $res=Reserva::latest('id')->first();
                $res_item['id_reserva']=$res->id;
                $res_item['id_user']=Auth::User()->id;
                Reserva_item::create($res_item);
            }
        }
        $arquivo=str_split(file_get_contents("events.json"));
        for($i=0;$i<3;$i++){
            array_pop($arquivo);
        }
        $editado="";
        foreach($arquivo as $array){
            $editado= $editado.$array;
        }
        file_put_contents("events.json","");
        fwrite($events,$editado);
        fwrite($events,','.json_encode($new).'] }');
        return redirect('/calendario');
    }
    
    public function todos(){

        $this->mudaDisp();
        $this->mudaDev();

        $dataRI= Reserva_item::all();
        if($dataRI != null && sizeof($dataRI)!= 0){
            for($i=0; $i<sizeof($dataRI);$i++){         
                $users[]=User::where('id',$dataRI[$i]->id_user)->get();
                $reservas[]=Reserva::where('id',$dataRI[$i]->id_reserva)->get();
                $materiais[]=Item::where('id',$dataRI[$i]->id_item)->get();
            }
    
            $dataU=array_flatten(array_unique($users));
            $dataR=array_flatten(array_unique($reservas));
            $dataM=array_flatten(array_unique($materiais));
            
            for($i=0;$i<sizeof($dataR);$i++){
                $locais[]=Local::where('id',$dataR[$i]->local)->get();
            }
    
            $dataL=array_flatten($locais);
            
            return view('reserva.agenda', compact('dataR','dataL','dataU','dataM'));
        }else{
            $dataR=null;
            return view('reserva.agenda',compact('dataR'));
        }

    }
    
    public function agendaUsuario(){
        
        $this->mudaDisp();
        $this->mudaDev();
        
        $dataU=Auth::User()->id;
        
        $dataRI= Reserva_item::where('id_user',$dataU)->get();
        if($dataRI != null && sizeof($dataRI)!= 0){
            for($i=0; $i<sizeof($dataRI);$i++){         
                $reservas[]=Reserva::where('id',$dataRI[$i]->id_reserva)->get();
                $materiais[]=Item::where('id',$dataRI[$i]->id_item)->get();
            }
    
            $dataR=array_flatten(array_unique($reservas));
            $dataM=array_flatten(array_unique($materiais));
            
            for($i=0;$i<sizeof($dataR);$i++){
                $locais[]=Local::where('id',$dataR[$i]->local)->get();
            }
    
            $dataL=array_flatten($locais);
            
            return view('reserva.agendaUsuario', compact('dataR','dataL','dataU','dataM'));
        }else{
            $dataR=null;
            return view('reserva.agendaUsuario',compact('dataR'));
        }
    }
    
    public function cancelarAgendamento(){
        
        $this->mudaDisp();
        $this->mudaDev();

        $idR= Route::getCurrentRoute()->parameters()['id'];
        $dataR= Reserva::where('id', $idR)->first();
        $dataL= Local::where('id',$dataR->local)->first();
        return view('reserva.cancelarAgendamento', compact('dataR','dataL'));
    }

    public function canceleAgendamento(){
        
        $this->mudaDisp();
        $this->mudaDev();

        $idR= Route::getCurrentRoute()->parameters()['id'];
        $ri= Reserva_item::where('id_reserva',$idR)->get();
        for($i=0;$i<sizeof($ri);$i++){
            $itens[]=Item::where('id',$ri[$i]->id_item)->first();
        }
        for($i=0;$i<sizeof($itens);$i++){
            $itens[$i]->status='Disponivel';
            $itens[$i]->save();
        }
        Reserva::where('id',$idR)->delete();
        Reserva_item::where('id_reserva',$idR)->delete();

        return redirect('/agendamento/agenda');
    }

    public static function mudaDisp(){
        
        date_default_timezone_set('America/Sao_Paulo');
        $res= Reserva::where('retirada',date("Y-m-d"))->get();

        if ($res!=null){

            for($i=0;$i<sizeof($res);$i++){
                $ri=Reserva_item::where('id_reserva',$res[$i]->id)->get();

                for($j=0;$j<sizeof($ri);$j++){
                    $data=$res[$i]->retirada." ".$res[$i]->hretirada;
                    
                    if(date("Y-m-d H:i:s") < $data){
                        $itens= Item::where('id',$ri[$j]->id_item)->first();

                        if($itens->status == 'Disponivel'){
                            $itens->status = 'Requisitado';
                            $itens->save();
                        }
                    }
                }
            }
        }
    }

    public static function mudaDev(){
        date_default_timezone_set('America/Sao_Paulo');
        
        $res= Reserva::where('devolucao',date("Y-m-d"))->get();
        if ($res!=null){

            for($i=0;$i<sizeof($res);$i++){
                $ri=Reserva_item::where('id_reserva',$res[$i]->id)->get();
                for($j=0;$j<sizeof($ri);$j++){
                    $data=$res[$i]->devolucao." ".$res[$i]->hdevolucao;
                    
                    if($data < date("Y-m-d H:i:s")){
                        $itens= Item::where('id',$ri[$j]->id_item)->first();
                        if($itens->status == 'Requisitado'){
                            $itens->status = 'Disponivel';
                            $itens->save();
                            $res[$i]->delete();
                            $ri[$j]->delete();
                        }
                    }
                }
            
            }    
        }
    }
}