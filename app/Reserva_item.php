<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva_item extends Model{
    public $table = "reserva_items";
    public $timestamps = false;    
    
    protected $fillable = [
        'id_reserva',
        'id_item',
        'id_user'
    ];
}