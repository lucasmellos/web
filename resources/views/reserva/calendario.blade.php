@extends('layout.base')

@section ('title')
    <title>Controle | Calendário</title>
@stop

@section ('head')
<style>
    .modal-footer{
        text-align: center;
    }
    .modal-body{
        column-count: 2;
        border-bottom: 1px solid #e5e5e5;
    }
    .modal-body-2{
        margin-top: 25px;
        column-count: 2;
    }
    .input{
        margin-bottom: 5px;
        width: 100%;
    }
    .alert {
        opacity: 1;
        transition: opacity 0.6s; 
    }
    .alert {
        padding: 20px;
        background-color: #f44336; /* Red */
        color: white;
        margin-bottom: 15px;
    }

</style>
 
@stop

@section ('content')
<div id="container">
    <div style="width:100%; max-width:100%; display:inline-block;">
        <div class="monthly" id="mycalendar"></div>
    </div>
    <div class="modal fade" id="novoEvento">
        <div class="modal-dialog">
            <div class="alert" align="center" style="display: none;" id="erro">
                <span>Item já utilizado nesta data por outro agendamento! Favor selecionar outra.</span> 
            </div>
            <div class="modal-content">
                <form id="dados" name="form" class="form-horizonal" method="POST" action="/calendario" autocomplete="off">{{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" style='text-align: center;'>Criar nova reserva - {{Auth::User()->name}}</h4>
                    </div>
                    <div class="modal-body">                
                        <input class=" form-control form-group" name="name" placeholder="Título do evento" id='name' autofocus required>
                        <select class=" form-control form-group" name="materiais" id='materiais' required>
                            @foreach($materiais as $material)
                            <option value="{{$material->id}}" >{{$material->nome}}</option>
                            @endforeach
                        </select>
                        <select class="form-group form-control" name="local" required>
                            @foreach($locais as $local)
                                <option value="{{$local->id}}">{{$local->nome}}</option>
                            @endforeach
                        </select>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input class=" form-control form-group" name="quant" placeholder="Quantidade" type="number" id='quant' max="" required> 
                    </div>
                    <div class="modal-body-2" id="m2">            
                        <input class=" form-control form-group" name="startdate" placeholder="Data de início" type="date" value="" id='startdate' required>
                        <input class=" form-control form-group" name="enddate" placeholder="Data de término" type="date" id='enddate' required>
                        <input class=" form-control form-group" name="starttime" placeholder="Hora de início" type="time" id='starttime' required>
                        <input class=" form-control form-group" name="endtime" placeholder="Hora de término" type="time" id='endtime' required> 
                    </div> 
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default" name="enviar" value="enviar" id="enviar">Enviar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>      
        </div>
    </div>
</div>    
@stop

@section('script')
<script type="text/javascript">
	$.getScript('js/monthly.js', function() {

		$('#mycalendar').monthly({
			mode: 'event',
			jsonUrl: 'events.json',
			dataType: 'json'
			//xmlUrl: 'events.xml'
		});
                
	});
</script>

<script type="text/javascript">
    $('#materiais, #quant, #startdate, #enddate, #starttime, #endtime, #enviar').on('focus, change, click, mouseover',function(){

        var materiais= document.getElementById('materiais').value;
        var startdate= document.getElementById('startdate').value;
        var enddate= document.getElementById('enddate').value;
        var quantidade= document.getElementById('quant').value;

        $.ajax({
            method: "POST",
            url: "/calendario-disp",
            data: {itens: materiais, inicio: startdate, fim: enddate, _token: "{{ csrf_token() }}"},
                       
            success: function(retorno){
                document.getElementById('m2').disabled=false;                                
                document.getElementById('m2').style.display='';

                document.getElementById('startdate').disabled=false;                                
                document.getElementById('startdate').style.display='';
                
                document.getElementById('enddate').disabled=false;
                document.getElementById('enddate').style.display='';
                
                document.getElementById('enviar').disabled=false;
                document.getElementById('enviar').style.display='';
                
                document.getElementById('erro').style.display='none';
            },
            error: function(){
                document.getElementById('m2').disabled=true;                                
                document.getElementById('m2').style.display='none';

                document.getElementById('startdate').disabled=true;                                
                document.getElementById('startdate').style.display='none';
                
                document.getElementById('enddate').disabled=true;
                document.getElementById('enddate').style.display='none';
                
                document.getElementById('enviar').disabled=true;
                document.getElementById('enviar').style.display='none';
                
                document.getElementById('erro').style.display='block';
            }
        });
        
        $.ajax({
            method: "get",
            url: "/calendario-quant",
            data: {itens: materiais, _token: "{{ csrf_token() }}"},
            
            success: function(retorno){
                var valor= (JSON.parse(retorno).length);
                $('#quant').attr('max',valor);
            },
            error: function(retorno){
                var valor= (JSON.parse(retorno.responseText).length);
                $('#quant').attr('max',valor);
            }
        });
    });
</script>
@stop