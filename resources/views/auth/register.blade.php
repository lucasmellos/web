@extends('layout.baseLogin')
@section('content')

<form class="form-horizontal col-lg-3 col-md-4 col-sm-4" role="form" method="POST" action="{{ route('register') }}">
{{ csrf_field() }}

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="email" class="col-md-4 control-label">Nome</label>
    <div class="col-md-8">
        <input id="name" type="text" class="form-control" placeholder="Nome" name="name" value="{{ old('name') }}" required autofocus>
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="email" class="col-md-4 control-label">Sobrenome</label>
    <div class="col-md-8">
        <input id="name" type="text" class="form-control" placeholder="Sobrenome" name="lastname" value="{{ old('lastname') }}" required autofocus>

        @if ($errors->has('lastname'))
            <span class="help-block">
                <strong>{{ $errors->first('lastname') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="email" class="col-md-4 control-label">E-Mail</label>
    <div class="col-md-8">
        <input id="email" type="email" class="form-control" placeholder="Endereço de E-mail" name="email" value="{{ old('email') }}" required>

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    <label for="email" class="col-md-4 control-label">Senha</label>
    <div class="col-md-8">
        <input id="password" type="password" placeholder="Senha" class="form-control" name="password" required>

        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-md-4"></div>
    <div class="col-md-8">
        <input id="password-confirm" type="password" placeholder="Confirmar Senha" class="form-control" name="password_confirmation" required>
    </div>
</div>

<div class="form-group">
    <div style="text-align: center;">
        <button type="submit" class="btn btn btn-primary" style="margin-right: 5px">
            Cadastrar
        </button>
        <a href="/" class="btn btn btn-default">Cancelar</a>
    </div>                           
</div>                       
@endsection
